<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\Transaction;
use App\Product;

class Client extends Model
{
    protected $fillable = [
        'name','email'
    ];

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function transactions(){
        return $this->hasMany(Transaction::class);
    }
    
    public function available(){
        $products = Product::all();

        $todas = [];
        foreach($products as $product){
            //array_push($total,nuevo);
            $orders = Order::where([
                ['product_id','=',$product->id],
                ['client_id','=',$this->id]
            ])->get();
            $obj = [
                'product'=>['id'=>$product->id,'name'=>$product->name,'value'=>$product->value],
                'amount'=>0
            ];
            foreach($orders as $order){
                $obj['amount'] += $order->amount;
            }
            if($obj['amount']>0){
                $transactions = Transaction::where([
                    ['product_id','=',$product->id],
                    ['client_id','=',$this->id]
                ])->get();
                foreach($transactions as $transaction){
                    $obj['amount'] -= $transaction->amount;
                }
                if($obj['amount']>0){
                    array_push($todas,$obj);
                }
            }
        }
        return $todas;
    }

    public function hasEnough($product,$amount){
        $orders = Order::where([
            ['product_id','=',$product->id],
            ['client_id','=',$this->id]
        ])->get();
        
        $responce = false;

        $available = 0;
        foreach($orders as $order){
            $available += $order->amount;
        }
        
        if($available > 0){
            $transactions = Transaction::where([
                ['product_id','=',$product->id],
                ['client_id','=',$this->id]
            ])->get();
            foreach($transactions as $transaction){
                $available -= $transaction->amount;
            }
            $responce = $available >= $amount; 
        }
        return $responce;
    }
}

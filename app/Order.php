<?php

namespace App;

use App\Client;
use App\Product;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable =[ 
        'number','amount'
    ];

    public function client(){
    	return $this->belongsTo(Client::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }

}

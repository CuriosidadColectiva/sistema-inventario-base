<?php

namespace App;

use App\User;
use App\Client;
use App\Product;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable =[ 
        'amount'
    ];
    
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    public function client() {
        return $this->belongsTo(Client::class);
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }
}

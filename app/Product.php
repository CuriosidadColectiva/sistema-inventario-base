<?php

namespace App;

use App\Order;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'value'
    ];

    function orders(){
        return $this->hasMany(Order::class);
    }
}

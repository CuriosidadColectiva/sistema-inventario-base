<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::latest()->paginate(20);
  
        return view('clients.index',compact('clients'));
           // ->with('i', (request()->input('page', 1) - 1) * 20);
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clients.create');
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'second_name'=>'required',
            'email' => 'required',
        ]);
  
        Client::create($request->all());
   
        return redirect()->route('clients.index')
                        ->with('success','Client created successfully.');
    }
   
    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        $availables = $client->available();
        return view('clients.show',['client'=>$client,'availables'=>$availables]);
    }
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $Client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('clients.edit',compact('client'));
    }
  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $request->validate([
            'name' => 'required',
            'second_name'=>'required',
            'email' => 'required',
        ]);
  
        $client->update($request->all());
  
        return redirect()->route('clients.index')
                        ->with('success','Client updated successfully');
    }
  
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();
  
        return redirect()->route('clients.index')
                        ->with('success','Client deleted successfully');
    }

    public function filter(Request $request){
        $responce = null;
        if ($request->has('param') && strlen ($request->param)>0 ) {
            $clients = Client::query()->where(
                'name','LIKE',"%{$request->param}%"
            )->orWhere(
                'email','LIKE',"%{$request->param}%"
            )->paginate(20);
            return view('clients.index',compact('clients'));
        }else{
            $responce = redirect()->route('clients.index')->withErrors(['Sin parametros de busqueda']);
        }
        return $responce;
    }
}

<?php
namespace App\Http\Controllers;
use Auth;
use App\User;
use App\Client;
use Illuminate\Http\Request;

class JogoController extends Controller
{
    public function login()
    {
        return view('default.index');
    }

    public function doLogin(Request $request){
        $request->validate(['id'=>'required']);

        if(Auth::loginUsingId($request->id)){
            return redirect('/');
        }else{
            return redirect('register')->with('success','Aun no tienes una cueta, ingresa estos datos adicionales.');
        }
    }

    public function register(){
        return view('default.register');
    }

    public function doRegister(Request $request){
        $request->validate([
            'id'=>'required | numeric | digits_between:9,16 ',
            'name'=>'required',
            'email'=>'required | email'
        ]);
        
        if(!Auth::loginUsingId($request->id)){
            $user = new User;
            $user->id = $request->id;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = '';
            $user->save();
            Auth::loginUsingId($user->id);
        }
        return redirect('find');
    }

    public function doLogout(){
        Auth::logout();
        return redirect('login');
    }

    public function dashboard(){
        return redirect('clients');
    }

    public function find(Request $request){
        $responce = null;
        if($request->has('finding')){
            $clients = Client::query()->where(
                $request->by,'LIKE',"%{$request->value}%"
            )->paginate(20);
            return view('clients.index',compact('clients'));
        }else{
            $responce = view('default.find');
        }
        return $responce;
    }

}

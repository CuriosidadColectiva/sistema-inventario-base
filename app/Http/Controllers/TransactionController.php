<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\User;
use App\Client;
use App\Product;

use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user' => 'required',
            'product'=>'required',
            'client'=>'required',
            'amount' => 'required'
        ]);
        
        $client = Client::find($request->client);
        $product = Product::find($request->product);
        $responce = null;
        if($client->hasEnough($product,$request->amount)){
            $user = User::find($request->user);
            

            $transaction = new Transaction;
            $transaction->amount = $request->amount;

            $transaction->client()->associate($client);
            $transaction->user()->associate($user);
            $transaction->product()->associate($product);
            $transaction->save();
            $msg = $product->name;
            if(strpos($product->name, '5 uds') !== false) {
                $msg = substr_replace($product->name ,"",-5);
            }
            $responce = back()->with('success','Se han descontado '.$transaction->amount.' '.$msg.' con exito');
        }else{
            $responce = back()->withErrors(['La cantidad a entregar no puede ser mayor a la disponible.']);
        }
   
        return $responce;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}

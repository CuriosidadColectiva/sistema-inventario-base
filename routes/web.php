<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'guest'], function () {
    Route::get('/','JogoController@login');    
    Route::get('/login','JogoController@login')->name('login');
    Route::post('/login','JogoController@doLogin')->name('doLogin');
    Route::get('/register','JogoController@register')->name('register');
    Route::post('/register','JogoController@doRegister')->name('doRegister');
});



$router->group(['middleware' => 'auth'], function() {
    Route::get('/','JogoController@dashboard');
    Route::any('/find','JogoController@find')->name('find');
    Route::any('/logout','JogoController@doLogout')->name('doLogout');
    Route::resource('products','ProductController');
    Route::get('clients/find','ClientController@filter')->name('clients.find');
    Route::resource('clients','ClientController');
    Route::resource('orders','OrderController');
    Route::resource('transactions','TransactionController');
});

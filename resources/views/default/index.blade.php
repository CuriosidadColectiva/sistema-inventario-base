@extends('../layout')
 
@section('content')

    <div style="max-width:330px;margin: 100px auto;">
        <form class="form-signin"action="{{ route('doLogin') }}" method="POST">
            @csrf
            <h1 class="h3 mb-3 font-weight-normal">Identificate</h1>
            <label for="inputEmail" class="sr-only">Numero de cedula</label>
            <input type="text" name='id' id="inputEmail" class="form-control" placeholder="Cedula" required autofocus>
            <br>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
        </form>
        <a href="/register">No tienes cuenta aun?</a>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
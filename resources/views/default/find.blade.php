@extends('../layout')
 
@section('content')
<br><br>
<br><br>
    <form  action="{{ route('find') }}" method="POST">
    @csrf
        <input type='hidden' value="asdf" name='finding'/>
        <div class="form-group">
            <input name='value' type="text" class="form-control" placeholder="Buscando" required autofocus>
        </div>
        <select name='by' class="form-control form-control-xs">
            <option value="name">Nombre</option>
            <option value="email">Email</option>
        </select>
        <br/>
        <center>
            <button class="btn-success btn" type="submit">Buscar</button>
        </center>
    </form>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
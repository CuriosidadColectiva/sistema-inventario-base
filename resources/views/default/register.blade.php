@extends('../layout')
 
@section('content')

    <div style="max-width:330px;margin: 100px auto;">
        <form class="form-signin"action="{{ route('doRegister') }}" method="POST">
            @csrf
            <h1 class="h3 mb-3 font-weight-normal">Registrate</h1>
            <label for="inputId" class="sr-only">Numero de cedula</label>
            <input type="text" name='id' id="inputId" class="form-control" placeholder="Cedula" required autofocus>
            <br>
            <label for="inputName" class="sr-only">Nombre</label>
            <input type="text" name='name' id="inputName" class="form-control" placeholder="Nombre completo" required>
            <br>
            <label for="inputEmail" class="sr-only">Email</label>
            <input type="text" name='email' id="inputEmail" class="form-control" placeholder="Email" required>
            <br>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
        </form>
        <a href="/">No deberias estar aca?</a>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
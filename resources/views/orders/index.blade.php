@extends('../layout')
 
@section('content')

<div class="float-right">
    @if(Auth::user()->isAdmin)
        <a class="btn btn-success" href="{{ route('orders.create') }}">Crear nueva orden</a>
    @endif
</div>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Lista de ordenes existentes</h2>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>Id</th>
            <th>Producto</th>
            <th>Cantidad</th>
            <th>Cliente</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($orders as $order)
        <tr>
            <td>{{ $order->id }}</td>
            <td><a class="btn btn-info" href="{{ route('products.show',$order->product->id) }}"> {{ $order->product->name }} </a></td>
            <td>{{ $order->amount }}</td>
            <td><a class="btn btn-info" href="{{ route('clients.show',$order->client->id) }}"> {{ $order->client->name." ".$order->client->second_name }} </a></td>
            <td>
                <form action="{{ route('orders.destroy',$order->id) }}" method="POST">
                    <center>
                    <a class="btn btn-info" href="{{ route('orders.show',$order->id) }}">Mostar</a>
                    @if(Auth::user()->isAdmin)
                    <a class="btn btn-primary" href="{{ route('orders.edit',$order->id) }}">Editar</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Borrar</button>
                    @endif
                    </center>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    
    {!! $orders->links() !!}
      
@endsection
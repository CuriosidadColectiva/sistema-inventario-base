<!DOCTYPE html>
<html>
<head>
    <title>JoGo - Rappi</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
@if (Auth::check())
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#"><small>{{Auth::user()->name}}</small></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link" href="/find">Buscar</a>
            <a class="nav-item nav-link" href="/clients">Clientes</a>
            <a class="nav-item nav-link" href="/products">Productos</a>
            <!--<a class="nav-item nav-link" href="/orders">Ordenes</a>-->
        </div>
    </div>

    <a class="form-inline my-2 my-lg-0 nav-link" style="color:dc3545;"href="/logout">Salir</a>

</nav>
@endif
<div class="container">

<br>
    @yield('content')
</div>
<p class="mt-5 mb-3 text-muted float-right" style="padding-right:50px;">Curiosidad Colectiva - &copy; 2019  </p>
</body>
</html>
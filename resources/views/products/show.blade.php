@extends('../layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Viendo producto</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $product->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Value:</strong>
                {{ $product->value }}
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
        <h5> Compras realizadas </h5>
            <table class="table table-bordered table-hover table-sm " cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="th-sm">Producto</th>
                        <th class="th-sm">Valor</th>
                        <th class="th-sm">Cantidad</th>
                        <th class="th-sm">Cliente</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($product->orders as $order)
                    <tr>
                        <td>{{ $order->product->name }}</td>
                        <td>{{ $order->product->value }}</td>
                        <td>{{ $order->amount }}</td>
                        <td><a href="{{route('clients.show',$order->client->id)}}">{{ $order->client->name }}</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
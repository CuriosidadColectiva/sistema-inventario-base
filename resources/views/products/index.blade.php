@extends('../layout')
 
@section('content')
    @if(Auth::user()->isAdmin)
    <div class="float-right">
        <a class="btn btn-success" href="{{ route('products.create') }}"> Crear nuevo producto</a>
    </div>
    @endif
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Lista de productos</h2>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Value</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($products as $product)
        <tr>
            <td>{{ $product->id }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->value }}</td>
            <td>
                <form action="{{ route('products.destroy',$product->id) }}" method="POST">
                    <center>
                    <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">Mostrar</a>
                    @if(Auth::user()->isAdmin)
                    <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}">Editart</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Borrar</button>
                    @endif
                    </center>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    
    {!! $products->links() !!}
      
@endsection
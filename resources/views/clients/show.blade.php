@extends('../layout')
@section('content')

    <div class="float-right">
        <a class="btn btn-primary" href="{{ route('clients.index') }}">Listado de clientes</a>
    </div>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Cliente: {{ $client->name }} </h2>
                <hr>
            </div>
            
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Identificador:</strong>
                {{ $client->email }}
            </div>
        </div>
    </div>
        
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Hemos encontrado algunos problemas.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
        <h5> Producto disponible </h5>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Producto</th>
                        <th>Valor</th>
                        <th>Cantidad</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($availables as $product)
                    <tr>
                        <td>{{ $product['product']['name'] }}</td>
                        <td>{{ $product['product']['value'] }}</td>
                        <td>{{ $product['amount'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class ="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                <h4> Entregar producto </h4>
                </div>
            </div>
            <div class="row">
                <form action="{{ route('transactions.store') }}" method="POST">
                    @csrf
                    <input type="hidden" name="client" value="{{ $client->id }}">
                    <input type="hidden" name="user" value="{{ Auth::user()->id }}">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <select name="product" class="custom-select custom-select-xs mb-3">
                            @foreach ($availables as $product)
                                <tr>
                                    <option value="{{ $product['product']['id'] }}">{{ $product['product']['name'] }}</option>
                                </tr>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="amount" placeholder="Cantidad a entregar" aria-label="Cantidad a entregar" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn-success btn" type="submit">Entregar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
        <br>
        <hr>
        
        </div>
        
    </div>
   
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4">
        <h5> Compras realizadas </h5>
            <table class="table table-bordered table-hover table-sm " cellspacing="0" width="100%">
                <thead>
                    <tr>
			<th class="th-sm">#</th>
                        <th class="th-sm">Producto</th>
                        <th class="th-sm">Valor</th>
                        <th class="th-sm">Cantidad</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($client->orders as $order)
                    <tr>
                        <td>{{ $order->number }}</td>
                        <td>{{ $order->product->name }}</td>
                        <td>{{ $order->product->value }}</td>
                        <td>{{ $order->amount }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        
        <div class="col-xs-8 col-sm-8 col-md-8">
        <h5> Retiros realizados </h5>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Transaccion</th>
                        <th>Producto</th>
                        <th>Valor</th>
                        <th>Cantidad</th>
                        <th>Hora</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($client->transactions as $transaction)
                    <tr>
                        <td>{{ $transaction->id }}</td>
                        <td>{{ $transaction->product->name }}</td>
                        <td>{{ $transaction->product->value }}</td>
                        <td>{{ $transaction->amount }}</td>
                        <td>{{ $transaction->created_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
@endsection

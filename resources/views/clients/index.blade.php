@extends('../layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-right">
                <form  method="GET" action="{{ route('clients.find') }}">
                    @csrf
                    <div class="form-row">
                        <div class="col">
                        <input type="text" size="35" name="param" class="form-control" value="{{ app('request')->input('param') }}" placeholder="Nombre o Email">
                        </div>
                        <div class="col">
                        <button type="submit" class="btn btn-primary">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="pull-left">
                <h2>Lista de clientes existentes</h2>
            </div>
            <!--<div class="pull-right">
                <a class="btn btn-success" href="{{ route('clients.create') }}"> Create New Client</a>
            </div>-->
        </div>
    </div>
   
    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Hemos encontrado algunos problemas.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered table-hover">
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Identificador</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($clients as $client)
        <tr>
            <td>{{ $client->id }}</td>
            <td>{{ $client->name }}</td>
            <td>{{ $client->email }}</td>
            <td>
                <form action="{{ route('clients.destroy',$client->id) }}" method="POST">
                <center>
   
                    <a class="btn btn-info" href="{{ route('clients.show',$client->id) }}">Mostar</a>
    
                    @if(Auth::user()->isAdmin)
                    <a class="btn btn-primary" href="{{ route('clients.edit',$client->id) }}">Editar</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Borrar</button>
                    @endif
                </center>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    
    {!! $clients->links() !!}
      
@endsection

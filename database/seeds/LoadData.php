<?php

use Illuminate\Database\Seeder;

use App\Client;
use App\Product;
use App\Order;

class LoadData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo PHP_EOL.PHP_EOL;
        $handle = fopen("database.csv", "r");
        if ($handle) {
            fgets($handle);//Discart first line
            $lineas = 0;
            $clientes = 0;
            $productos = 0;
            while (($line = fgets($handle)) !== false) {
                $datas = preg_split ("/\,/", $line);
	        if(sizeof($datas)>3){
                    //echo $lineas.PHP_EOL;
                    ++$lineas;
                    $cNombre = $datas[0].' '.$datas[1];
                    $client = Client::where('name', $cNombre)->first();
                    if(!$client){
                        ++$clientes;
                        $client = new Client;
			$client->email = $datas[2];
                $client->name = $cNombre;
                        $client->save();
                    }

                    $pNombre = $datas[3];
                    $pValor = 1;
	            $product = Product::where([
                        ['name','=',$pNombre],
                        ['value','=',$pValor]
                    ])->first();
                    if(!$product){
                        ++$productos;
                        $product = new Product;
		        $product->name = $pNombre;
                        $product->value = $pValor;
                        $product->save();
                    }

                    $order = new Order;
		    $order->number = intval($datas[4]);
                    $order->amount = intval($datas[5]);
                    $order->client()->associate($client);
                    $order->product()->associate($product);
                    $order->save();
                }
            }
            echo "Ordenes procesadas: ".$lineas.PHP_EOL;
            echo "Clientes procesados: ".$clientes.PHP_EOL;
            echo "Productos procesados: ".$productos.PHP_EOL;
            fclose($handle);
        } else {
            echo "Archivo no encontrado";
        } 
        echo PHP_EOL.PHP_EOL;
    }
}

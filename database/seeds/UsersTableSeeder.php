<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
        ->insert([
            'id' => '114900770',
            'name' => "Alex Vargas Benamburg",
            'email' => '',
            'password' => "",
            'isAdmin' => true
        ]);
        
    }
}

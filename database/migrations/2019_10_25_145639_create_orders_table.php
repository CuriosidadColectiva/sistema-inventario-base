<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('number')->unsigned()->nullable();
            
            $table->bigInteger('client_id')->unsigned(); 
            $table->foreign('client_id')
                ->references('id')->on('clients')
                ->onDelete('cascade');
            
            $table->bigInteger('product_id')->unsigned(); 
            $table->foreign('product_id')
                    ->references('id')->on('products')
                    ->onDelete('cascade');
            
            $table->bigInteger('amount');

            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
